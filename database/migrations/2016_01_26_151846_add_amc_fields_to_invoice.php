<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAmcFieldsToInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->boolean('recurring_fee_applicable');
            $table->float('recurring_fee');
            $table->integer('recurring_fee_interval');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
           $table->dropColumn(['recurring_fee_applicable','recurring_fee','recurring_fee_interval']); 
        });
    }
}
