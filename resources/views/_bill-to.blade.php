	<div class="bluebg" style="width:100%;">BILL TO: <?php $clients = App\Models\Client::all(); ?>
      <span style="float:right" class="no-print">Select Client </span>
      <select id="client-name-dropdown" name="client_ID" class="form-control no-print">
      <option disabled="disabled" selected="selected">Select Client</option>
      @foreach($clients as $client)
      	<option value={{ $client->id }}>{{$client->name}}</option>
      @endforeach
      </select>
    </div>
	
