<table class="table">
    <thead>
    <th>Description</th>
			<th>Amount</th>
            <th>Invoice ID</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($items as $item)
        <tr>
            <td>{!! $item->description !!}</td>
			<td>{!! $item->amount !!}</td>
            <td>{!! $item->invoice_id!!}
            <td>
                <a href="{!! route('items.edit', [$item->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('items.delete', [$item->id]) !!}" onclick="return confirm('Are you sure wants to delete this Item?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
