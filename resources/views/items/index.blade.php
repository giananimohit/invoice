@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Items</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('items.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($items->isEmpty())
                <div class="well text-center">No Items found.</div>
            @else
                @include('items.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $items])


    </div>
@endsection