<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $item->description !!}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{!! $item->amount !!}</p>
</div>

<div class="form-group">
    {!! Form::label('invoice_id', 'Invoice ID:') !!}
    <p>{!! $item->invoice_id !!}</p>
</div>
