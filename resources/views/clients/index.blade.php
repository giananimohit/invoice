@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Clients</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('clients.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($clients->isEmpty())
                <div class="well text-center">No Clients found.</div>
            @else
                @include('clients.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $clients])


    </div>
@endsection