<table class="table">
    <thead>
    <th>Name</th>
			<th>Address</th>
			<th>Phno</th>
			<th>Email</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($clients as $client)
        <tr>
            <td>{!! $client->name !!}</td>
			<td>{!! $client->address !!}</td>
			<td>{!! $client->phno !!}</td>
			<td>{!! $client->email !!}</td>
            <td>
                <a href="{!! route('clients.edit', [$client->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('clients.delete', [$client->id]) !!}" onclick="return confirm('Are you sure wants to delete this Client?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
