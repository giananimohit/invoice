@extends('app')

@section('content')

    <div class="container">

        <div class="row">
            <h1 class="pull-left">Details</h1>
        </div>

        <div class="row">
            @if($invoices->isEmpty())
                <div class="well text-center">No Items found.</div>
            @else
                <h3>Invoices</h3>
                <table class="table">
                    <thead>
                    <th>Invoice no.</th>
                    <th>Amount</th>
                    <th>Paid?</th>
                    </thead>
                    <tbody>
                    @foreach($invoices as $invoice)
                        <tr>
                            <td>{!! $invoice->id !!}</td>
                            <td>{!! $invoice->items->sum('amount'); !!}</td>
                            <td>{!! ($invoice->paid==1)? 'yes' : 'no' !!}
                    
                            
                        </tr>
                    @endforeach
                    His unpaid invoices are {{ $unpaid_invoices->pluck('id') }}<br/>
                    His total unpaid sum is {{ $total_unpaid }}
                    </tbody>
                </table>
            @endif
        </div>

        


    </div>
@endsection