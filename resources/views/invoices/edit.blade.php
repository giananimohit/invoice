@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($invoice, ['route' => ['invoices.update2', $invoice->id], '_method' => 'patch']) !!}

        @include('invoices.fields')

    {!! Form::close() !!}
</div>
@endsection
