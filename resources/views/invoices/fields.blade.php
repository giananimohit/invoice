<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('title', 'Title:') !!}
	{!! Form::text('title', $invoice->title, ['class' => 'form-control']) !!}
</div>

<!-- Client Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('client_ID', 'Client Id:') !!}
	{!! Form::text('client_ID', null, ['class' => 'form-control']) !!}
</div>
<?php //dd($invoice); ?>

<div class="clearfix"></div>
<!-- Comments Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('comments', 'Comments:') !!}
	{!! Form::textarea('comments', null, ['class' => 'form-control', 'style' => 'width:200%;']) !!}
</div>

<div class="clearfix"></div>
<!-- Sent Field -->
<div style="margin-top:20px">
	<div class="form-group col-sm-6 col-lg-4">
	    {!! Form::label('sent', 'Sent:') !!}
		<div class="radio-inline">
			<label>
				{!! Form::radio('sent', '1') !!} Yes
			</label>
		</div>
		<div class="radio-inline">
			<label>
				{!! Form::radio('sent', '0') !!} No
			</label>
		</div>
	</div>

	<!-- Taxes Applicable Field -->
	<div class="form-group col-sm-6 col-lg-4">
	    {!! Form::label('taxes_applicable', 'Taxes Applicable:') !!}
		<div class="radio-inline">
			<label>
				{!! Form::radio('taxes_applicable', '1') !!} Yes
			</label>
		</div>
		<div class="radio-inline">
			<label>
				{!! Form::radio('taxes_applicable', '0') !!} No
			</label>
		</div>
	</div>

	<div class="form-group col-sm-6 col-lg-4">
	    {!! Form::label('paid', 'Invoice Status:') !!}
		<div class="radio-inline">
			<label>
				{!! Form::radio('paid', '0') !!} Unpaid
			</label>
		</div>
		<div class="radio-inline">
			<label>
				{!! Form::radio('paid', '1') !!} Paid
			</label>
		</div>
		<div class="radio-inline">
			<label>
				{!! Form::radio('paid', '2') !!} Closed
			</label>
		</div>
	</div>

	<div class="form-group col-sm-6 col-lg-4">
	    {!! Form::label('recurring_fee_applicable', 'Reccuring Fee Applicable:') !!}
		<div class="radio-inline">
			<label>
				{!! Form::radio('recurring_fee_applicable', '1') !!} Yes
			</label>
		</div>
		<div class="radio-inline">
			<label>
				{!! Form::radio('recurring_fee_applicable', '0') !!} No
			</label>
		</div>
	</div>
</div>
<?php $date = \Carbon\Carbon::parse($invoice->date)->format('m/d/Y'); ?>
<div class="clearfix"></div>
<div style="margin-top:20px">
	<div class="form-group col-sm-6 col-lg-4">
	    {!! Form::label('date', 'Date:') !!}
		{!! Form::date('date', \Carbon\Carbon::parse($date), ['class' => 'form-control']) !!}
	</div>

	<div class="form-group col-sm-6 col-lg-4">
	    {!! Form::label('sent_at', 'Sent at:') !!}
		{!! Form::date('sent_at', $invoice->sent_at, ['class' => 'form-control']) !!}
	</div>

	<div class="form-group col-sm'-6 col-lg-4">
	    {!! Form::label('paid_at', 'Paid at:') !!}
		{!! Form::date('paid_at', $invoice->paid_at, ['class' => 'form-control']) !!}
	</div>
	<!-- Invoice Number Field -->
	<div class="form-group col-sm-6 col-lg-4">
	    {!! Form::label('invoice_number', 'Invoice Number:') !!}
		{!! Form::text('invoice_number', null, ['class' => 'form-control']) !!}
	</div>

	<div class="form-group col-sm-6 col-lg-4">
	    {!! Form::label('recurring_fee', 'Recurring Fee:') !!}
		{!! Form::text('recurring_fee', $invoice->recurring_fee, ['class' => 'form-control']) !!}
	</div>

	<div class="form-group col-sm-6 col-lg-4">
	    {!! Form::label('recurring_fee_interval', 'Recurring Fee Interval:') !!}
		{!! Form::text('recurring_fee_interval', $invoice->recurring_fee_interval, ['class' => 'form-control']) !!}
	</div>
	<!-- Submit Field -->
	<div class="form-group col-sm-12">
	    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
	</div>
</div>