<!-- Invoice Number Field -->
<div class="form-group">
    {!! Form::label('invoice_id', 'Invoice ID:') !!}
    <p>{!! $invoice->id !!}</p>
</div>

<!-- Invoice Number Field -->
<div class="form-group">
    {!! Form::label('invoice_number', 'Invoice Number:') !!}
    <p>{!! $invoice->invoice_number !!}</p>
</div>

<!-- Client Id Field -->
<div class="form-group">
    {!! Form::label('client_ID', 'Client Id:') !!}
    <p>{!! $invoice->client_ID !!}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{!! $invoice->date !!}</p>
</div>

<!-- Comments Field -->
<div class="form-group">
    {!! Form::label('comments', 'Comments:') !!}
    <p>{!! $invoice->comments !!}</p>
</div>

<!-- Sent Field -->
<div class="form-group">
    {!! Form::label('sent', 'Sent:') !!}
    <p>{!! $invoice->sent !!}</p>
</div>

<!-- Taxes Applicable Field -->
<div class="form-group">
    {!! Form::label('taxes_applicable', 'Taxes Applicable:') !!}
    <p>{!! $invoice->taxes_applicable !!}</p>
</div>

<div class="form-group">
    {!! Form::label('sent_at', 'Sent at:') !!}
    <p>{!! $invoice->date !!}</p>
</div>

<div class="form-group">
    {!! Form::label('paid', 'Paid:') !!}
    <p>{!! $invoice->paid !!}</p>
</div>

<div class="form-group">
    {!! Form::label('paid_at', 'Paid at:') !!}
    <p>{!! $invoice->paid_at !!}</p>
</div>