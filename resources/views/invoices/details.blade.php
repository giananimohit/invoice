@extends('app')

@section('content')

    <div class="container">

        <div class="row">
            <h1 class="pull-left">Details</h1>
        </div>

        <div class="row">
                
                <table class="table">
                    <thead>
                        <th>Invoice no.</th>
                        <th>Issued to</th>
                        <th>Due Date</th>
                        <th>Sent?</th>
                        <th>Taxes Applicable?</th>
                        <th>Created at</th>
                        <th>Updated at</th>
                        <th>Sent at</th>
                        <th>Paid?</th>
                        <th>Paid at</th>
                    </thead>
                    <tbody>
              
                    <tr>
                        <td>{!! $invoice->id !!}</td>
                        <td>{!! $client->name !!}</td>
                        <td>{!! $due_date !!}</td>
                        <td>{!! $sent !!}</td>
                        <td>{!! $taxes_applicable !!}</td>
                        <td>{!! $created_at !!}</td>
                        <td>{!! $updated_at !!}</td>
                        <td>{!! $sent_at !!}</td>
                        <td>{!! $paid !!}</td>
                        <td>{!! $paid_at !!}</td>
                    </tr>
                    </tbody>
                </table>
            
        </div>
    </div>
@endsection