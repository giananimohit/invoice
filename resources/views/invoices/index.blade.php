@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')
        {!! Form::open(array('action' => 'InvoiceController@sendDefaulterEmail', 'class' => 'form-horizontal')) !!}
            <button type="submit" class="btn btn-default">Send email</button>
        {!! Form::close() !!}
        <div class="row">
            <h1 class="pull-left">Invoices</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('invoices.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($invoices->isEmpty())
                <div class="well text-center">No Invoices found.</div>
            @else
                @include('invoices.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $invoices])


    </div>
@endsection