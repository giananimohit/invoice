<table class="table">
    <thead>
    <th>Invoice ID</th>
    <th>Job Number</th>
            
			<th>Client ID</th>
			<th>Date</th>
			<th>Comments</th>
			<th>Sent</th>
			<th>Taxes Applicable</th>
            <th>Sent at</th>
            <th>Paid</th>
            <th>Paid at</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($invoices as $invoice)
        <tr>
            <td><a href="{{route('see',['id' => $invoice->id])}}">{!! $invoice->id !!}</a></td>
            <td>{!! $invoice->invoice_number !!}</td>
			<td>{!! $invoice->client_ID !!}</td>
			<td>{!! Carbon\Carbon::parse($invoice->date)->format('d/m/Y') !!}</td>
			<td>{!! $invoice->comments !!}</td>
			<td>{!! ($invoice->sent) ? "Yes":  "No" !!}</td>
			<td>{!! ($invoice->taxes_applicable) ? "Yes" : "No" !!}</td>
            <td>{!! Carbon\Carbon::parse($invoice->sent_at)->format('d/m/Y') !!}</td>
            @if($invoice->paid == 0)
            <td>No</td>
            @elseif($invoice->paid == "1")
            <?php echo $invoice->paid; ?>
            <td>Yes</td>
            @elseif($invoice->paid == "2")
            <?php echo "entered closed" ?>
            <td>Closed</td>
            @endif
            <td>{!! Carbon\Carbon::parse($invoice->paid_at)->format('d/m/Y') !!}</td>
            <td>
                <a href="{!! route('invoices.edit', [$invoice->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('invoices.delete', [$invoice->id]) !!}" onclick="return confirm('Are you sure wants to delete this Invoice?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
