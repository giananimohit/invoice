@include('_cssinclude')
<body>
@if($new == 1)
{!! Form::open(array('action' => 'ItemController@dieDump', 'class' => 'form-horizontal')) !!}

@else
{!! Form::model($invoice, ['route' => ['invoices.update', $invoice->id], 'method' => 'patch']) !!}

@endif
<div class="admin-bar no-print text-center">	
<div>Title : <input type="text" name="title" id="title" value="{{$title}}"></div>
<div>Tax applicable? <input type="radio" name="taxes_applicable" value="1" {{$taxes_applicable ? 'checked' : ''}}>Yes<input type="radio" name="taxes_applicable" value="0" {{$taxes_applicable ? '' : 'checked'}}>No</div>
<div>Bill To: <?php $clients = App\Models\Client::all(); ?>
      <select id="client-name-dropdown" class="form-control no-print">
      <option disabled="disabled" selected="selected">Select Client</option>
      @foreach($clients as $client)
      	<option value={{ $client->id }}>{{$client->name}}</option>
      @endforeach
      </select>
</div>
<input type="hidden" id="hidden-client-id" name="client_ID" value=""> <!-- check why i haven't used name in select field" -->
<div>Recurring? <input type="radio" name="recurring" value="1" {{$recurring ? 'checked' : ''}}>Yes<input type="radio" name="recurring" value="0" {{$recurring ? '' : 'checked'}}>No</div>
</div>
<div class="container">

<div class="billing-details">
	<div class="lcol">
	@include('_our-address')

	
	<span id="client-name">{!! $client->name !!}</span><br/>
	<span id="client-address">{!! $client->address !!}</span><br/>
	<span id="client-phno">{!! $client->phno !!}</span><br/>
	
	<input type="hidden" name="id" value="{{$invoice_number}}">
	</div>
	<div class="ccol">
	sales@itsalive.in<br/>
	Pan Card No.: AADFI2139R<br/>
	ST No. : AADFI2139RSD002<br/>
	ST Category: Information Technology Software &amp; Design Service<br/>
	
	</div>
	<div class="rcol">
	<span style="color: #6666CC; font-size:40px; float:right;">INVOICE</span><br/><br/><br/>
	<div class="row"><span class="font-bold">DATE: </span><span class="invoice-details">{{ $date }}</span> </div>
	<div class="row"><span class="font-bold">INVOICE #: </span><span id="invoice-number" class="invoice-details">{{ $invoice_number }}</span> </div>
	<div class="row"><span class="font-bold">Customer ID:  </span><span id="client-id" class="invoice-details"> {{ $invoice->client_ID }}</span> </div>

	
	</div>

</div>
<div class="clearfix"></div>
<div class="job-number text-center print-margin-left"><input type="text" id="job-number" value="{{$job_number}}" name="job_number"></div><br/>


<input type="hidden" value="{{$tax}}" id="hidden-tax-percent">
<div class="job-description">
	<table id="main-table" class="table">
		<thead>
			<tr class="bluebg">
				<th width="90%" style="border-right: 2px solid #ddd; text-align:center">DESCRIPTION <div id="add-lineitem" style="float:right; height:20px;"><img src="/images/plus_sign.png" style="max-height:100%; cursor:pointer;" class="no-print"></div></th>
				<th width="10%" style="text-align:center;">AMOUNT</th>
			</tr>
		</thead>
		<tbody>
		
		@if(isset($line_item_count))
		<input type="hidden" value="{{$line_item_count}}" id="hidden-line-item-count">
		<input type="hidden" name="new_line_item_count" id="new-line-item-count" value="0">
		@for($i = 1; $i<=$line_item_count; $i++)
			<tr class="lineitem{{$i}}">
				<td name="lineitems[{{$i}}][description]" style="border-right: 2px solid #ddd; padding:0; height:40px;"><input type="text" name="lineitems[{{$i}}][description]" style="width:100%; height:100%; border:none;" value="{{$line_items[$i-1]['description']}}"></td>

				<td name="lineitems[{{$i}}][amount]" style="text-align:right; padding:0; height:40px;"><input type="text" class="amount" id="{{$line_items[$i-1]->id}}" name="lineitems[{{$i}}][amount]" style="text-align:right; height:100%; border:none; width:100%;" value="{{$line_items[$i-1]['amount']}}"></td>
				
			</tr>
			<input type="hidden" name="lineitem_ids[{{$i}}]" value="{{$line_items[$i-1]->id}}">

		@endfor
		
		@else
			<input type="hidden" value="1" id="hidden-line-item-count">
			<tr class="lineitem1">
				<td name="lineitems[1][description]" style="border-right: 2px solid #ddd; padding:0; height:40px;"><input type="text" name="lineitems[1][description]" style="width:100%; height:100%; border:none;"></td>
				<td name="lineitems[1][amount]" style="text-align:right; padding:0; height:40px;"><input type="text" class="amount" id="amount1" name="lineitems[1][amount]" style="text-align:right; height:100%; border:none; width:100%;" value="0"></td>
			</tr>

		@endif
	
		
			<tr>
				<td colspan="2" style="padding:0;"><table width="100%" style="border:none;" class="table custom-tab1">
					<thead>
						<tr style="border-top: 1px solid #ddd;">
							<th width="70%" style="border-right: 2px solid #ddd; background-color:#C0C0C0;">OTHER COMMENTS</th>
							<th width="20%" style="border-right: 2px solid #ddd;">SUBTOTAL</th>
							<th id="subtotal" width="10%" style="text-align:right; background-color: #E4E8F3;">{{ 0 }}</th>
						</tr>
					</thead>
					<tbody>
                          <tr>
                          @if($new==1)
                            <td rowspan="2" style="border-right: 2px solid #ddd; padding: 0px;" contenteditable="true"><textarea name="comments" id="other-comments" style="border:none; width:100%; height:100px; text-align:left;">1) Total payment due on 1st September, 2015.
2) Please include the invoice number on your cheque.
3) Make all cheques payable to "It's Alive".</textarea></td>
						   @else
							<td rowspan="2" style="border-right: 2px solid #ddd; padding: 0px;" contenteditable="true"><textarea name="comments" id="other-comments" style="border:none; width:100%; height:100px; text-align:left;">{{$comments}}</textarea></td>
						   @endif
                            <td style="border-right: 2px solid #ddd;">Service Tax ({{$tax}}%)</td>
                            <td id="service-tax" style="text-align:right;">0.00</td>
                          </tr>
                          <tr>
                            <td style="border-right: 2px solid #ddd; border-top: 1px solid #ddd;">TOTAL</td>
                            <td id="total" style="text-align:right; border-top: 1px solid #ddd;">0.00</td>
                          </tr>
                        </tbody>
				</table></td>
			</tr>
		</tbody>
	</table>
	 {!! Form::submit('Save', array('class' => 'submit-form no-print btn btn-default form-control')) !!}
{!! Form::close() !!}
</div>

<div class="postscript text-center">
If you have any questions about this invoice, please contact <br/>
Paramveer Bhatti, +919820860909, paramveer@itsalive.in<br/><br/>

<span style="font-weight:bold; font-size:20px;">Thank you for your business!</span>
</div>

</div>
</body>
@include('_jsinclude')