<script src="/js/jquery-2.1.4.js"></script>
<script src="/js/jquery-1.11.1.min.js"></script>
<script src="/js/bootstrap.js"></script>
<script>

var tax = $('#hidden-tax-percent').val();
var i=parseInt($('#hidden-line-item-count').val());
var newItems = 0;
console.log(i);
var subtotal=0;
$('document').ready(function(){
	$('input:radio[name=tax-applicable]').click(function() {
	  var val = $('input:radio[name=tax-applicable]:checked').val();
	  tax = val=='yes'? tax : 0;
	  console.log(val);
	  console.log(tax);
	});
	
	$('#add-lineitem').click(function(){
		i+=1;
		$('<tr class="lineitem'+i+'"> \
				<td id="lineitems[' + i + '][description]" style="border-right: 2px solid #ddd;  padding:0; height:40px;"><input type="text" name="lineitems['+i+'][description]" style="width:100%; height:100%; border:none;"></td> \
				<td class="amount-box" id="lineitems[' + i + '][amount]" style="text-align:right; padding:0; height:40px;"><input type="text" id="amount'+i+'" name="lineitems['+i+'][amount]" style="width:100%; height:100%; border:none; text-align:right;" value="0" class="amount"></td> \
			</tr>').insertAfter('.lineitem'+(i-1)); 
		newItems+=1;
		$('#new-line-item-count').val(newItems);
	});
	$('#client-name-dropdown').change(function(){
		$.post("/returnClient",{ "id": $(this).val(), "_token": $("[name='_token']").val() }, function(data){ 
		$("#client-name").html(data.name);
		$("#client-address").html(data.address);
		$("#client-id").html(data.id);	
		$('#hidden-client-id').val(data.id);
		});
	});

	var stotal = 0;
	$('tbody').focusout(function(){
		
		$('.amount').each(function(){
			stotal+=parseInt($(this).val());
		});
		$('#subtotal').text(stotal);
		$('#service-tax').text(parseInt(tax/100*stotal));
		$('#total').text(parseInt(stotal+(tax/100*stotal)));
		console.log(stotal);
		stotal=0;
	});
	console.log(stotal);

	
	// function AppViewModel(){
	// 	this.amount = ko.observable("0");
	// }
	// ko.applyBindings(new AppViewModel());
	
});
</script>
</html>