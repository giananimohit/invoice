<!-- Tax Percent Field -->
<div class="form-group">
    {!! Form::label('tax_percent', 'Tax Percent:') !!}
    <p>{!! $tax->tax_percent !!}</p>
</div>

<!-- Year Field -->
<div class="form-group">
    {!! Form::label('year', 'Year:') !!}
    <p>{!! $tax->year !!}</p>
</div>

