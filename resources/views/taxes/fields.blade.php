<!-- Tax Percent Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('tax_percent', 'Tax Percent:') !!}
	{!! Form::text('tax_percent', null, ['class' => 'form-control']) !!}
</div>

<!-- Year Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('year', 'Year:') !!}
	{!! Form::text('year', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
