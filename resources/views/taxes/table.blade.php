<table class="table">
    <thead>
    <th>Tax Percent</th>
			<th>Year</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($taxes as $tax)
        <tr>
            <td>{!! $tax->tax_percent !!}</td>
			<td>{!! $tax->year !!}</td>
            <td>
                <a href="{!! route('taxes.edit', [$tax->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('taxes.delete', [$tax->id]) !!}" onclick="return confirm('Are you sure wants to delete this Tax?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
