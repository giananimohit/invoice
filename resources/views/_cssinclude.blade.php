<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>invoice</title>
    {{-- <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css"> --}}
    <link href="/css/_bootstrap.css" rel="stylesheet">
    {{-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> --}}
    <style>
    .container{
        padding: 3%;
    }
    .billing-details{
        height: 350px;
        /*padding-top: 3%;*/
    }
    .lcol{
        width:30%;
        /*position: absolute;*/
        float: left;
        padding-left: 3%;

    }
    .rcol{
        width:30%;
        /*position: absolute;*/
        float: left;        
        margin-left: 15px;
        -webkit-print-color-adjust:exact;
    }
    .ccol{
        width:30%;
        margin-top: 5%;
        /*position: absolute;*/
        float: left;
    }
    .invoice-details{
        float: right;

    }
    .job-number{
        font-weight: bold;
    }
    
    
    .height {
        min-height: 200px;
    }


    .custom-tab1>tbody>tr>td {
        padding: 5px;
        vertical-align: middle;
        border: none;
    }
    .postscript{
        margin-top: 50px;

    }
    .bluebg{
        background-color: #3B4E87;
        color:#fff;
        font-weight: bold;
    }
    .table {
        margin: 0;
        border: 2px solid #ddd;
    }
    @media print{
        .no-print{
            display: none;
        }
        .print-margin-left{

        }
    }
    .font-bold{
        font-weight:bold;
    }
    .admin-bar{
        background-color: #E4E8F3;
        height: 40px;
        font-size: 17px;
    }
    .admin-bar div{
        padding: 20px;
    }
    .admin-bar div, .admin-bar div span, .admin-bar div select{
        display: inline;
    }
    .admin-bar div select{
        width:inherit;
    }
    </style>
</head>
