@extends('app')
@section('content')
<div class="container">
    <div class="row">
        <h1 class="pull-left">Details</h1>
    </div>
    <div class="row">
        <table class="table">
            <thead>
                <th>Invoice no.</th>
                <th>Issued to</th>
                <th>Due Date</th>
                <th>Title</th>
                <th>Amount</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach($invoices as $invoice)
                <?php
                $client = \App\Models\Client::find($invoice->client_ID);
                $due_date = \Carbon\Carbon::parse($invoice->date)->format('d/m/Y');
                ?>
                <tr>
                    <td>{!! $invoice->id !!}</td>
                    <td>{!! $client->name !!}</td>
                    <td>{!! $due_date !!}</td>
                    <td>{!! $invoice->title !!}</td>
                    <td>{!! $invoice->items->sum('amount') !!}</td>
                    <td>
                        <a href="{!! route('invoices.edit', [$invoice->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                        <a><i class="glyphicon glyphicon-euro" data-toggle="modal" data-target="#myModal"></i></a>
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                                    </div>
                                    <div class="modal-body">
                                        ...
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="{{route('sendReminderEmail',['id' => $invoice->id])}}" onclick="return confirm('Are you sure you want to send a reminder email?')"><i class="glyphicon glyphicon-envelope"></i></a>
                        <a href="{!! route('invoices.delete', [$invoice->id]) !!}" onclick="return confirm('Are you sure you want to delete this Invoice?')"><i class="glyphicon glyphicon-remove"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection