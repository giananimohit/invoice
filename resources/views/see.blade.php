@include('_cssinclude')
<body>
<div class="container">
<div class="billing-details">
	<div class="lcol">
	@include('_our-address')

	<span id="client-name">{!! $client->name !!}</span><br/>
	<span id="client-address">{!! $client->address !!}</span><br/>
	<span id="client-phno">{!! $client->phno !!}</span><br/>
	</div>
	<div class="ccol">
	sales@itsalive.in<br/>
	Pan Card No.: AADFI2139R<br/>
	ST No. : AADFI2139RSD002<br/>
	ST Category: Information Technology Software &amp; Design Service<br/>
	</div>
	<div class="rcol">
	<span style="color: #6666CC; font-size:40px; float:right;">INVOICE</span><br/><br/><br/>
	<div class="row"><span class="font-bold">DATE: </span><span class="invoice-details">{{ $date }}</span> </div>
	<div class="row"><span class="font-bold">INVOICE #: </span><span id="invoice-number" class="invoice-details">{{ $invoice_number }}</span> </div>
	<div class="row"><span class="font-bold">Customer ID:  </span><span id="client-id" class="invoice-details"> {{ $invoice->client_ID }}</span> </div>
	</div>
</div>
<div class="job-number text-center print-margin-left">{{$job_number}}</div><br/>
<div class="job-description">
	<table id="main-table" class="table">
		<thead>
			<tr class="bluebg">
				<th width="90%" style="border-right: 2px solid #ddd; text-align:center">DESCRIPTION <div id="add-lineitem" style="float:right; height:20px;"></div></th>
				<th width="10%" style="text-align:center;">AMOUNT</th>
			</tr>
		</thead>
		<tbody>
			@for($i = 1; $i<=$line_item_count; $i++)
				<tr class="lineitem1">
					<td name="lineitems[{{$i}}][description]" style="border-right: 2px solid #ddd; padding:0; height:40px; margin-top:2px;">{{$line_items[$i-1]['description']}}</td>

					<td name="lineitems[{{$i}}][amount]" style="text-align:right; padding:0; height:40px;">{{$line_items[$i-1]['amount']}}</td>
					
				</tr>
			@endfor
			<tr>
				<td colspan="2" style="padding:0;"><table width="100%" style="border:none;" class="table custom-tab1">
					<thead>
						<tr style="border-top: 1px solid #ddd;">
							<th width="70%" style="border-right: 2px solid #ddd; background-color:#C0C0C0;">OTHER COMMENTS</th>
							<th width="20%" style="border-right: 2px solid #ddd;">SUBTOTAL</th>
							<th id="subtotal" width="10%" style="text-align:right; background-color: #E4E8F3;">{{ $amount }}</th>
						</tr>
					</thead>
					<tbody>
                          <tr>
                            <td rowspan="2" style="border-right: 2px solid #ddd; padding: 0px;" contenteditable="true"><textarea name="comments" id="other-comments" style="border:none; width:100%; height:100px; text-align:left;">{{$comments}}</textarea></td>
                            <td style="border-right: 2px solid #ddd;">Service Tax ({{$tax}}%)</td>
                            <td id="service-tax" style="text-align:right;"><?php $stax = round($tax/100*$amount,0); echo $stax; ?></td>
                          </tr>
                          <tr>
                            <td style="border-right: 2px solid #ddd; border-top: 1px solid #ddd;">TOTAL</td>
                            <td id="total" style="text-align:right; border-top: 1px solid #ddd;">{{$stax+$amount}}</td>
                          </tr>
                        </tbody>
				</table></td>
			</tr>
			</tbody>
	</table>
</div>
<div class="postscript text-center">
If you have any questions about this invoice, please contact <br/>
Paramveer Bhatti, +919820860909, paramveer@itsalive.in<br/><br/>

<span style="font-weight:bold; font-size:20px;">Thank you for your business!</span>
</div>

</div>
</body>
</html>
