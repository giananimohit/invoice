<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="css/_bootstrap.css" rel="stylesheet">
    <script src="js/jquery-2.1.4.js"></script>
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.js"></script>
    </head>
<body style="color:black;">
    <div class="container">
        <div class="content">
            {!! Form::open(array('action' => 'ItemController@htmlDisplay', 'class' => 'form-horizontal')) !!}
                <div class="form-group">

                    <div class="col-sm-10">
                      <?php $clients = App\Models\Client::all(); ?>
                      Select Client <select name="client" class="form-control">
                      @foreach($clients as $client)
                      <option value={{ $client->id }}>{{$client->name}}</option>
                      @endforeach
                  </select>
              </div>
          </div>
          <div id="lineitems">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                  {!! Form::text('line_items[0][description]', null, array('class' => 'form-control', 'placeholder' => 'Description')) !!}
              </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Amount</label>
            <div class="col-sm-10">
              {!! Form::text('line_items[0][amount]', null, array('class' => 'form-control', 'placeholder' => 'Amount')) !!}
          </div>
      </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10" id="lineitems-parent">
        <div id="add-item" class="btn btn-default">Add item</div>
    </div>
</div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        {!! Form::submit('Get PDF', array('class' => 'btn btn-default form-control')) !!}
    </div>
</div>

{!! Form::close() !!}
</div>
</div>
</body>
<script>
    var lineItemCount = 0;
    $('document').ready(function(){
        $('#add-item').click(function(){
            lineItemCount+=1;
            var toappend='<div><div class="form-group"><label for="" class="col-sm-2 control-label">Description</label> \
                <div class="col-sm-10"> \
                  <input type="text" name="line_items[' + lineItemCount + '][description]" class="form-control" /> \
              </div> \
          </div> \
          <div class="form-group"> \
            <label for="" class="col-sm-2 control-label">Amount</label> \
            <div class="col-sm-10"> \
              <input type="text" name="line_items[' + lineItemCount + '][amount]" class="form-control"/> \
          </div> \
      </div> \
  </div>';
            $('#lineitems').append(toappend);
            
        });
    });
</script>
    </html>