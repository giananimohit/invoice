<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Invoice extends Model
{
    
	public $table = "invoices";
    

	public $fillable = [
	    "invoice_number",
		"client_ID",
		"date",
		"comments",
		"sent",
		"taxes_applicable",
		"sent_at",
		"paid",
		"paid_at",
		"title",
		"recurring_fee_applicable",
		"recurring_fee",
		"recurring_fee_interval"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "invoice_number" => "string",
		"client_ID" => "string",
		"date" => "date",
		"comments" => "string",
		"sent" => "boolean",
		"taxes_applicable" => "boolean",
		"sent_at" => "date",
		"paid" => "boolean",
		"paid_at" => "date",
		"title" => "string",
		"recurring_fee_applicable" => "boolean",
		"recurring_fee" => "string",
		"recurring_fee_interval" => "string"
    ];

	public static $rules = [
	    
	];
	public function items(){
		return $this->hasMany('App\Models\Item');
	}
	public function client(){
		return $this->belongsTo('App\Models\Client','client_ID');
	}

}
