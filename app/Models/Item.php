<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Item extends Model
{
    
	public $table = "items";
    

	public $fillable = [
	    "description",
		"amount",
		"invoice_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "description" => "string",
		"amount" => "integer",
		"invoice_id" => "integer"
    ];

	public static $rules = [
	    
	];

	public function invoice(){
		return $this->belongsTo('App\Models\Invoice','invoice_id');
	}
}
