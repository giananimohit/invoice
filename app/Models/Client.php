<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Client extends Model
{
    
	public $table = "clients";
    

	public $fillable = [
	    "name",
		"address",
		"phno",
		"email"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name" => "string",
		"address" => "string",
		"phno" => "string",
		"email" => "string"
    ];

	public static $rules = [
	    
	];

	public function invoices(){
		return $this->hasMany('App\Models\Invoice');
	}

}
