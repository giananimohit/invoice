<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Tax extends Model
{
    
	public $table = "taxes";
    

	public $fillable = [
	    "tax_percent",
		"year"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "tax_percent" => "float",
		"year" => "integer"
    ];

	public static $rules = [
	    
	];

}
