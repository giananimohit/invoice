<?php
Route::group(['middleware' => 'auth'], function () {


    Route::get('/','InvoiceController@htmlDisplay');

    Route::get('invoices/pending','InvoiceController@pending');

    Route::get('clients/{id}/details','ClientController@details',compact('id'));
    Route::get('invoices/{id}/details','InvoiceController@details',compact('id'));

    Route::get('invoices/{id}/modify','InvoiceController@modify',compact('id'));

    Route::post('invoices/{id}/update2','InvoiceController@update2',compact('id'))->name('invoices.update2');

    Route::resource('clients', 'ClientController');

    Route::get('clients/{id}/delete', [
        'as' => 'clients.delete',
        'uses' => 'ClientController@destroy',
    ]);

    Route::resource('invoices', 'InvoiceController');

    Route::get('invoices/{id}/delete', [
        'as' => 'invoices.delete',
        'uses' => 'InvoiceController@destroy',
    ]);

    Route::resource('items', 'ItemController');

    Route::get('items/{id}/delete', [
        'as' => 'items.delete',
        'uses' => 'ItemController@destroy',
    ]);

    Route::get('htmlinvoice','InvoiceController@htmlDisplay');

    Route::post('dieDump','ItemController@dieDump');

    Route::post('email','InvoiceController@sendDefaulterEmail');

    Route::post('returnClient','ItemController@returnClient');

    Route::post('submitComments','ItemController@submitComments');

    Route::get('invoices/{id}/see', ['as' => 'see', 'uses' => 'InvoiceController@see']);

    Route::get('invoice/{id}/sendReminderEmail', ['as' => 'sendReminderEmail', 'uses' => 'InvoiceController@sendReminderEmail']);

    Route::get('invoice/{id}/paymentReceived', ['as' => 'paymentReceived', 'uses' => 'InvoiceController@paymentReceived']);


});

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::resource('taxes', 'TaxController');

Route::get('taxes/{id}/delete', [
    'as' => 'taxes.delete',
    'uses' => 'TaxController@destroy',
]);
