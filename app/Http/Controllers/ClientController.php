<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateClientRequest;
use App\Http\Requests\UpdateClientRequest;
use App\Libraries\Repositories\ClientRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ClientController extends AppBaseController
{

	/** @var  ClientRepository */
	private $clientRepository;

	function __construct(ClientRepository $clientRepo)
	{
		$this->clientRepository = $clientRepo;
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the Client.
	 *
	 * @return Response
	 */
	public function index()
	{
		$clients = $this->clientRepository->paginate(10);

		return view('clients.index')
			->with('clients', $clients);
	}

	/**
	 * Show the form for creating a new Client.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('clients.create');
	}

	/**
	 * Store a newly created Client in storage.
	 *
	 * @param CreateClientRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateClientRequest $request)
	{
		$input = $request->all();

		$client = $this->clientRepository->create($input);

		Flash::success('Client saved successfully.');

		return redirect(route('clients.index'));
	}

	/**
	 * Display the specified Client.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$client = $this->clientRepository->find($id);

		if(empty($client))
		{
			Flash::error('Client not found');

			return redirect(route('clients.index'));
		}

		return view('clients.show')->with('client', $client);
	}

	/**
	 * Show the form for editing the specified Client.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$client = $this->clientRepository->find($id);

		if(empty($client))
		{
			Flash::error('Client not found');

			return redirect(route('clients.index'));
		}

		return view('clients.edit')->with('client', $client);
	}

	/**
	 * Update the specified Client in storage.
	 *
	 * @param  int              $id
	 * @param UpdateClientRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateClientRequest $request)
	{
		$client = $this->clientRepository->find($id);

		if(empty($client))
		{
			Flash::error('Client not found');

			return redirect(route('clients.index'));
		}

		$this->clientRepository->updateRich($request->all(), $id);

		Flash::success('Client updated successfully.');

		return redirect(route('clients.index'));
	}

	/**
	 * Remove the specified Client from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$client = $this->clientRepository->find($id);

		if(empty($client))
		{
			Flash::error('Client not found');

			return redirect(route('clients.index'));
		}

		$this->clientRepository->delete($id);

		Flash::success('Client deleted successfully.');

		return redirect(route('clients.index'));
	}

	public function details($id)
	{
		$client = $this->clientRepository->find($id);
		$invoices = $client->invoices;
		// $unpaid_invoices = DB::table('clients')->where('')
		$unpaid_invoices = $client->invoices->where('paid',false);
		$total_unpaid = 0;
		foreach($unpaid_invoices as $i){
			$total_unpaid += $i->items->sum('amount');
		}
		// $unpaid_invoices = $client->invoices->where('paid',false)->map(function(){
		// 	return id;
		// });
		// dd($unpaid_invoices);
		return view('clients.details', compact('invoices','unpaid_invoices','total_unpaid'));	
	}
}
