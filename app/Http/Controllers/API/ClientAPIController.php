<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\ClientRepository;
use App\Models\Client;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ClientAPIController extends AppBaseController
{
	/** @var  ClientRepository */
	private $clientRepository;

	function __construct(ClientRepository $clientRepo)
	{
		$this->clientRepository = $clientRepo;
	}

	/**
	 * Display a listing of the Client.
	 * GET|HEAD /clients
	 *
	 * @return Response
	 */
	public function index()
	{
		$clients = $this->clientRepository->all();

		return $this->sendResponse($clients->toArray(), "Clients retrieved successfully");
	}

	/**
	 * Show the form for creating a new Client.
	 * GET|HEAD /clients/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Client in storage.
	 * POST /clients
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Client::$rules) > 0)
			$this->validateRequestOrFail($request, Client::$rules);

		$input = $request->all();

		$clients = $this->clientRepository->create($input);

		return $this->sendResponse($clients->toArray(), "Client saved successfully");
	}

	/**
	 * Display the specified Client.
	 * GET|HEAD /clients/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$client = $this->clientRepository->apiFindOrFail($id);

		return $this->sendResponse($client->toArray(), "Client retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Client.
	 * GET|HEAD /clients/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified Client in storage.
	 * PUT/PATCH /clients/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var Client $client */
		$client = $this->clientRepository->apiFindOrFail($id);

		$result = $this->clientRepository->updateRich($input, $id);

		$client = $client->fresh();

		return $this->sendResponse($client->toArray(), "Client updated successfully");
	}

	/**
	 * Remove the specified Client from storage.
	 * DELETE /clients/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->clientRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Client deleted successfully");
	}
}
