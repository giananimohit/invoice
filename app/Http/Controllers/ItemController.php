<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateItemRequest;
use App\Http\Requests\UpdateItemRequest;
use App\Libraries\Repositories\ItemRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ItemController extends AppBaseController
{

	/** @var  ItemRepository */
	private $itemRepository;

	function __construct(ItemRepository $itemRepo)
	{
		$this->itemRepository = $itemRepo;
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the Item.
	 *
	 * @return Response
	 */
	public function index()
	{
		$items = $this->itemRepository->paginate(10);

		return view('items.index')
			->with('items', $items);
	}
	/**
	 * Show the form for creating a new Item.
	 *
	 * @return Response
	 */
	public function create(CreateItemRequest $request)
	{
		
		return view('items.create');
	}

	/**
	 * Store a newly created Item in storage.
	 *
	 * @param CreateItemRequest $request
	 *
	 * @return Response
	 */
	public function returnComments(CreateItemRequest $request){

	}
	public function dieDump(CreateItemRequest $request)
	{
		$input = $request->all();
		// dd($input);
		foreach($request->input('lineitems') as $i){
		$item = $this->itemRepository->create($i);
		$item->invoice_id = $request->input('id');
		$item->save();
		}
		$invoice = new \App\Models\Invoice;
		// dd($invoice);
		$invoice->id = $request->input('id');
		$invoice->invoice_number = $request->input('job_number');
		$invoice->client_ID = $request->input('client_ID');
		$invoice->comments = $request->input('comments');
		$invoice->sent = false;
		$invoice->taxes_applicable = ($request->input('taxes_applicable')==0? 0 : 1);
		$invoice->date = \Carbon\Carbon::now();
		$invoice->save();

		Flash::success('Invoice saved successfully.');
		return redirect(route('invoices.index'));
		
	}
	public function returnClient(CreateItemRequest $request)
	{
		$input = $request->all();
		// dd($input);
		return \App\Models\Client::find($input['id']);
	}

	public function store(CreateItemRequest $request)
	{
		//foreach($line_items as $li){
		$input = $request->all();
		//dd($request->input('line_items'));
		$item = $this->itemRepository->create($input);
		//}
		Flash::success('Invoice saved successfully.');

		return redirect(route('items.index'));
	}

	/**
	 * Display the specified Item.
	 *
	 * @param  int $id
	 *
	 * @return Responsep
	 */
	public function show($id)
	{
		$item = $this->itemRepository->find($id);

		if(empty($item))
		{
			Flash::error('Item not found');

			return redirect(route('items.index'));
		}

		return view('items.show')->with('item', $item);
	}

	/**
	 * Show the form for editing the specified Item.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$item = $this->itemRepository->find($id);

		if(empty($item))
		{
			Flash::error('Item not found');

			return redirect(route('items.index'));
		}

		return view('items.edit')->with('item', $item);
	}

	/**
	 * Update the specified Item in storage.
	 *
	 * @param  int              $id
	 * @param UpdateItemRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateItemRequest $request)
	{
		$item = $this->itemRepository->find($id);

		if(empty($item))
		{
			Flash::error('Item not found');

			return redirect(route('items.index'));
		}

		$this->itemRepository->updateRich($request->all(), $id);

		Flash::success('Item updated successfully.');

		return redirect(route('items.index'));
	}

	/**
	 * Remove the specified Item from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$item = $this->itemRepository->find($id);

		if(empty($item))
		{
			Flash::error('Item not found');

			return redirect(route('items.index'));
		}

		$this->itemRepository->delete($id);

		Flash::success('Item deleted successfully.');

		return redirect(route('items.index'));
	}
}
