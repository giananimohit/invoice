<?php namespace App\Http\Controllers;

use Mail;
use App\Http\Requests;
use App\Http\Requests\CreateInvoiceRequest;
use App\Http\Requests\UpdateInvoiceRequest;
use App\Libraries\Repositories\InvoiceRepository;
use App\Http\Requests\CreateItemRequest;
use App\Http\Requests\UpdateItemRequest;
use App\Libraries\Repositories\ItemRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use mikehaertl\wkhtmlto\Pdf;

class InvoiceController extends AppBaseController
{

	/** @var  InvoiceRepository */
	private $invoiceRepository;
	private $itemRepository;

	function __construct(InvoiceRepository $invoiceRepo, ItemRepository $itemRepo)
	{
		$this->invoiceRepository = $invoiceRepo;
		$this->itemRepository = $itemRepo;
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the Invoice.
	 *
	 * @return Response
	 */
	
	public function index()
	{
		$invoices = $this->invoiceRepository->paginate(10);

		return view('invoices.index')
			->with('invoices', $invoices);
	}

	public function sendDefaulterEmail()
	{
		$defaultedInvoices = \App\Models\Invoice::where('paid','=','false')->get();

		Mail::send('email', ['defaultedInvoices' => $defaultedInvoices], function ($m) {
            $m->from('hello@app.com', 'Your Application');
            $m->to('mohit@itsalive.in', 'Mohit Gianani')->subject('Your Reminder!');
        });
	}

	/**
	 * Show the form for creating a new Invoice.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('invoices.create');
	}

	/**
	 * Store a newly created Invoice in storage.
	 *
	 * @param CreateInvoiceRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateInvoiceRequest $request)
	{
		$input = $request->all();

		$invoice = $this->invoiceRepository->create($input);

		Flash::success('Invoice saved successfully.');

		return redirect(route('invoices.index'));
	}

	/**
	 * Display the specified Invoice.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$invoice = $this->invoiceRepository->find($id);

		if(empty($invoice))
		{
			Flash::error('Invoice not found');

			return redirect(route('invoices.index'));
		}

		return view('invoices.show')->with('invoice', $invoice);
	}

	/**
	 * Show the form for editing the specified Invoice.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	

	/**
	 * Update the specified Invoice in storage.
	 *
	 * @param  int              $id
	 * @param UpdateInvoiceRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateInvoiceRequest $request)
	{
		$invoice = $this->invoiceRepository->find($id);
		$this->invoiceRepository->updateRich($request->all(), $id);
		// dd($this);

		if(empty($invoice))
		{
			Flash::error('Invoice not found');

			return redirect(route('invoices.index'));
		}

		$lineitems = $request->input('lineitems');
		$lineitemids = $request->input('lineitem_ids');
		$line_item_count = sizeOf($lineitemids);
		$added_items = $request->input('new_line_item_count');
		
		for($i = 1; $i<=$line_item_count; $i++){
			$lineitem = \App\Models\Item::find($lineitemids[$i]);
			$lineitem->description = $lineitems[$i]['description'];
			$lineitem->amount = $lineitems[$i]['amount'];
			$lineitem->save();
		}
		
		$new_items = array_slice($lineitems, sizeOf($lineitems)-$added_items);
		
		foreach($new_items as $new_item){
			$item = $this->itemRepository->create($new_item);
			$item->invoice_id = $id;
			$item->save();
		}

		Flash::success('Invoice updated successfully.');

		return redirect(route('invoices.index'));
	}

	public function backendUpdate($id, UpdateInvoiceRequest $request)
	{
		$invoice = $this->invoiceRepository->find($id);
		$this->invoiceRepository->updateRich($request->all(),$id);
		
	}

	/**
	 * Remove the specified Invoice from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$invoice = $this->invoiceRepository->find($id);

		if(empty($invoice))
		{
			Flash::error('Invoice not found');

			return redirect(route('invoices.index'));
		}

		$this->invoiceRepository->delete($id);

		Flash::success('Invoice deleted successfully.');

		return redirect(route('invoices.index'));
	}

	public function details($id)
	{
		$invoice = $this->invoiceRepository->find($id);
		$client = \App\Models\Client::find($invoice->client_ID);

		$due_date = \Carbon\Carbon::parse($invoice->date)->format('d/m/Y');
		$comments = $invoice->comments;
		$sent = ($invoice->sent==1)? 'yes' : 'no';
		$taxes_applicable = ($invoice->taxes_applicable==1)? 'yes' : 'no';
		$created_at = \Carbon\Carbon::parse($invoice->created_at)->format('d/m/Y H:i:s');
		$updated_at = $invoice->updated_at;
		$sent_at = $invoice->sent_at;
		$paid = ($invoice->paid==1)? 'yes' : 'no';
		$paid_at = $invoice->paid_at;
		$items = $invoice->items;
		$amount = $items->sum('amount');

		return view('invoices.details',compact('invoice','paid','items','job_number','amount','client',
			'due_date','comments','sent','taxes_applicable','created_at','updated_at','sent_at','paid_at'));
	}

	public function pending()
	{
		// 'paid' = 0 means unpaid, 1 means paid, 2 means closed
		$invoices = \App\Models\Invoice::where('paid','0')->get();
		return view('pending',compact('invoices','amount'));
	}


	// First function to hit, displays the home page i.e the new invoice page
	public function htmlDisplay(CreateInvoiceRequest $request)
	{
		// $input = $request->all();
		// // dd($input);
		// foreach($request->input('line_items') as $i){
		// $item = $this->itemRepository->create($i);
		// }
		$client = new \App\Models\Client;
		$invoice = new \App\Models\Invoice;
		$created_at = \Carbon\Carbon::parse($invoice->created_at)->format('d/m/Y');
		$job_number = $invoice->invoice_number;
		$comments = $invoice->comments;
		if( \App\Models\Invoice::count() != 0){
			$invoice_number = \DB::table('invoices')->orderBy('id','desc')->first()->id+1;
		}
		else{
			$invoice_number = 1;
		}
		$taxes_applicable = 1;
		$line_items = $invoice->items;
		$line_item_count = 1;
		$current_year = \Carbon\Carbon::now()->year;	
		$tax = \App\Models\Tax::where('year','=',$current_year)->first()->tax_percent;
		$new = 1;
		$date = \Carbon\Carbon::now()->format('d/m/Y');
		$recurring = 0;
		$title = "";
		return view('inv',compact('request','client','invoice_number','invoice','job_number','taxes_applicable','created_at','comments','tax','line_items','new', 'date','recurring','title'));
	}

	public function edit($id)
	{
		$invoice = $this->invoiceRepository->find($id);
		$client = \App\Models\Client::find($invoice->client_ID);
		$job_number = $invoice->invoice_number;
		$comments = $invoice->comments;
		// dd($comments);
		$invoice_number = $invoice->id;
		$created_at = \Carbon\Carbon::parse($invoice->created_at)->format('d/m/Y');
		$taxes_applicable = ($invoice->taxes_applicable==1)? '1' : '0';
		// dd($taxes_applicable);
		$line_items = $invoice->items;
		$line_item_count = sizeOf($line_items);
		// dd($line_items[1]->id);
		$invoice_date = \Carbon\Carbon::parse($invoice->date);		
		$tax = \App\Models\Tax::where('year','=',$invoice_date->year)->first()->tax_percent;
		$new = 0;
		$date = \Carbon\Carbon::parse($invoice->date)->format('d/m/Y');
		$title = $invoice->title;
		$recurring = ($invoice->recurring_fee_applicable==1)? '1' : '0';
		return view('inv',compact('request','client','invoice_number','invoice','job_number','taxes_applicable','created_at','comments','tax','line_items','line_item_count','new','date','recurring','title'));
	}

	public function see($id)
	{
		$invoice = \App\Models\Invoice::find($id);
		$client = \App\Models\Client::find($invoice->client_ID);
		$invoice_number = $invoice->id;
		$job_number = $invoice->invoice_number;
		$line_items = $invoice->items;
		$line_item_count = sizeOf($line_items);
		$invoice_date = \Carbon\Carbon::parse($invoice->date);		
		$tax = \App\Models\Tax::where('year','=',$invoice_date->year)->first()->tax_percent;
		$data = compact('client','invoice','invoice_number','job_number','line_items','line_item_count','tax');
		$date = \Carbon\Carbon::parse($invoice->date)->format('d/m/Y');
		$items = $invoice->items;
		$amount = $items->sum('amount');
		$comments = $invoice->comments;
		// $pdf = \PDF::loadView('see', $data);
		// return $pdf->download('invoice.pdf');

		
		return view('see',compact('client','invoice','invoice_number','job_number','line_items','line_item_count','tax','date','amount','comments'));
		//$layout = view('see',compact('client','invoice','invoice_number','job_number','line_items','line_item_count','tax'));
		//$pdf = PDF::loadView('see', compact('client','invoice','invoice_number','job_number','line_items','line_item_count','tax'));
		//dd($pdf);
		
	}

	public function modify($id)
	{
		$invoice = $this->invoiceRepository->find($id);

		if(empty($invoice))
		{
			Flash::error('Invoice not found');

			return redirect(route('invoice.index'));
		}

		return view('invoices.edit')->with('invoice', $invoice);
	}

	// invoices.edit has a form which calls function update2

	public function update2($id, UpdateInvoiceRequest $request)
	{
		$invoice = $this->invoiceRepository->find($id);

		// dd($request->all());


		$this->invoiceRepository->updateRich($request->all(), $id);

		if(empty($invoice))
		{
			Flash::error('invoice not found');

			return redirect(route('invoices.index'));
		}


		Flash::success('Invoice updated successfully.');

		return redirect(route('invoices.index'));
	}

	public function sendReminderEmail($id)
	{
		$invoice = \App\Models\Invoice::find($id);
		$client = \App\Models\Client::find($invoice->client_ID);
		$invoice_number = $invoice->id;
		$job_number = $invoice->invoice_number;
		$line_items = $invoice->items;
		$line_item_count = sizeOf($line_items);
		$invoice_date = \Carbon\Carbon::parse($invoice->date);		
		$tax = \App\Models\Tax::where('year','=',$invoice_date->year)->first()->tax_percent;
		$data = compact('client','invoice','invoice_number','job_number','line_items','line_item_count','tax');
		$date = \Carbon\Carbon::parse($invoice->date)->format('d/m/Y');
		$items = $invoice->items;
		$amount = $items->sum('amount');
		$comments = $invoice->comments;

		Mail::send('see', compact('client','invoice','invoice_number','job_number','line_items','line_item_count','tax','date','amount','comments'), function ($m) {
            $m->from('hello@app.com', 'Your Application');
            $m->to('mohit@itsalive.in', 'Mohit Gianani')->subject('Your Reminder!');
        });
	}

	public function paymentReceived($id)
	{
		
	}
}
