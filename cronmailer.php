<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "invoice";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT * from invoices where paid = false";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "id: " . $row["id"]. " - Client ID: " . $row["client_ID"]. " "."<br>";
    }
    echo "The time is " . date("h:i:sa");
} else {
    echo "0 results";
}
$conn->close();


require 'vendor/autoload.php';
use Mailgun\Mailgun;

# Instantiate the client.
$mgClient = new Mailgun('key-f46fcc08991e27f0fbd48b067a141eb3');
$domain = "sandbox0f7eabdf90054af482dd4e62667b8768.mailgun.org";

# Make the call to the client.
$result = $mgClient->sendMessage("$domain",
                  array('from'    => 'Mailgun Sandbox <postmaster@sandbox0f7eabdf90054af482dd4e62667b8768.mailgun.org>',
                        'to'      => 'Mohit Gianani <mohit@itsalive.in>',
                        'subject' => 'Hello Mohit Gianani',
                        'text'    => 'Congratulations Mohit Gianani, you just sent an email with Mailgun!  You are truly awesome!  You can see a record of this email in your logs: https://mailgun.com/cp/log .  You can send up to 300 emails/day from this sandbox server.  Next, you should add your own domain so you can send 10,000 emails/month for free.'));
?>